# Usa un'immagine di base da Docker Hub che supporta C
FROM gcc:latest

# Copia il file C nel container
COPY hello_world.c /usr/src/app/

# Imposta il lavoro corrente nella directory dell'applicazione
WORKDIR /usr/src/app/

# Compila il programma
RUN gcc -o hello_world hello_world.c

# Comando predefinito quando il container viene avviato
CMD ["./hello_world"]
